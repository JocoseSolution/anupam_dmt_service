﻿using System;
using System.Collections.Generic;
using System.Text;

namespace anupam_dmt_libary
{
    public static class A2Z_DMT_Helper
    {
        public static string GetBankResponse(string agentId)
        {
            string response = string.Empty;
            try
            {
                A2ZServiceCredential credential = Common_Service.GetA2ZServiceCredential("Get_Bank");
                if (!string.IsNullOrEmpty(credential.Api_Token))
                {
                    string rowRequest = "{\"api_token\":\"" + credential.Api_Token + "\",\"userId\":\"" + credential.UserId + "\",\"secretKey\":\"" + credential.SecretKey + "\"}";
                    response = Common_Service.Postman(credential.PostType, credential.PostUrl, rowRequest, "Get_Bank", agentId, Common_Service.GenrateTrackId(), true);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return response;
        }
    }
}
