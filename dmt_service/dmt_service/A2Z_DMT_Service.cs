﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace anupam_dmt_libary
{
    public static class A2Z_DMT_Service
    {
        public static bool InsertBankDetail(string agentId)
        {
            bool bank_inserted = false;
            try
            {
                string response = A2Z_DMT_Helper.GetBankResponse(agentId);
                if (!string.IsNullOrEmpty(response))
                {
                    List<BankRoot> bankList = JsonConvert.DeserializeObject<List<BankRoot>>(response);
                    if (bankList != null && bankList.Count > 0)
                    {
                        foreach (var bank in bankList)
                        {
                            string query = "insert into T_DMT_BankList(bankid,bankname,ifsc,status) values(" + bank.id + ",'" + bank.name + "','" + bank.ifsc + "','" + bank.status + "')";
                            bool issuccess = A2ZDataBase.InsertUpdateDataBase(query);
                            if (issuccess)
                            {
                                bank_inserted = true;
                            }
                            else
                            {
                                bank_inserted = false;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return bank_inserted;
        }
    }
}
