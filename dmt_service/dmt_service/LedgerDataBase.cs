﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace anupam_dmt_libary
{
    public static class LedgerDataBase
    {
        #region [Connection Strings]
        public static SqlConnection MyAmdDBConnection = new SqlConnection(A2ZConfig.MyAmdDBConnectionString);

        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }

        public static void MyAmdOpenConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Closed)
            {
                MyAmdDBConnection.Open();
            }
        }
        public static void MyAmdCloseConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Open)
            {
                MyAmdDBConnection.Close();
            }
        }
        #endregion

        public static List<string> LedgerDebitCreditUtility(double Amount, string spkey, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType)
        {
            List<string> HS = new List<string>();

            try
            {
                HS = DebitCreditAFromLedger(Amount, AgentId, AgencyName, AccountID, IPAddress, Debit, Credit, BookingType, Remark, Uploadtype, shortRemark, Narration, TransType);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return HS;
        }

        private static List<string> DebitCreditAFromLedger(double Amount, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType)
        {
            List<string> HS = new List<string>();
            sqlCommand = new SqlCommand("SP_INSERTUPLOADDETAILS_TRANSACTION", MyAmdDBConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            sqlCommand.Parameters.AddWithValue("@Amount", Math.Abs(Amount));
            sqlCommand.Parameters.AddWithValue("@AgentId", AgentId);
            sqlCommand.Parameters.AddWithValue("@AgencyName", AgencyName);
            sqlCommand.Parameters.AddWithValue("@InvoiceNo", "");
            sqlCommand.Parameters.AddWithValue("@PnrNo", "");
            sqlCommand.Parameters.AddWithValue("@TicketNo", "");
            sqlCommand.Parameters.AddWithValue("@TicketingCarrier", "");
            sqlCommand.Parameters.AddWithValue("@YatraAccountID", "");
            sqlCommand.Parameters.AddWithValue("@AccountID", AccountID);
            sqlCommand.Parameters.AddWithValue("@ExecutiveID", "");
            sqlCommand.Parameters.AddWithValue("@IPAddress", IPAddress);
            sqlCommand.Parameters.AddWithValue("@Debit", Debit);
            sqlCommand.Parameters.AddWithValue("@Credit", Credit);
            sqlCommand.Parameters.AddWithValue("@BookingType", BookingType);
            sqlCommand.Parameters.AddWithValue("@Remark", Remark);
            sqlCommand.Parameters.AddWithValue("@PaxId", 0);
            sqlCommand.Parameters.AddWithValue("@Uploadtype", Uploadtype);
            sqlCommand.Parameters.AddWithValue("@YtrRcptNo", "");
            sqlCommand.Parameters.AddWithValue("@ID", 1);
            sqlCommand.Parameters.AddWithValue("@Status", "Confirm");
            sqlCommand.Parameters.AddWithValue("@Type", "Con");
            sqlCommand.Parameters.AddWithValue("@Rmk", shortRemark);
            sqlCommand.Parameters.AddWithValue("@SplStatus", 1);
            sqlCommand.Parameters.AddWithValue("@BankName", "");
            sqlCommand.Parameters.AddWithValue("@BankCode", "");
            sqlCommand.Parameters.AddWithValue("@Narration", Narration);
            sqlCommand.Parameters.AddWithValue("@TransType", TransType);
            sqlCommand.Parameters.AddWithValue("@ModuleType", "");
            sqlCommand.Parameters.Add("@Aval_Balance", SqlDbType.Float).Direction = ParameterDirection.Output;
            //sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 50000);
            sqlCommand.Parameters.Add("@result", SqlDbType.NVarChar, 500).Direction = ParameterDirection.Output;

            MyAmdOpenConnection();
            int isSuccess = sqlCommand.ExecuteNonQuery();
            double Aval_Balance = Convert.ToDouble(sqlCommand.Parameters["@Aval_Balance"].Value.ToString());
            string result = sqlCommand.Parameters["@result"].Value.ToString();
            MyAmdCloseConnection();

            HS.Add(result);
            HS.Add(Aval_Balance.ToString());

            return HS;
        }
    }
}
