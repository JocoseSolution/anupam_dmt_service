﻿using System;
using System.Collections.Generic;
using System.Text;

namespace anupam_dmt_libary
{
    public class A2ZServiceCredential
    {
        public string Department { get; set; }
        public string Api_Token { get; set; }
        public string UserId { get; set; }
        public string SecretKey { get; set; }
        public string PostUrl { get; set; }
        public string PostType { get; set; }
    }
    public class BankRoot
    {
        public int id { get; set; }
        public string name { get; set; }
        public string ifsc { get; set; }
        public int status { get; set; }
    }
}
