﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace anupam_dmt_libary
{
    public static class A2ZDataBase
    {
        #region [Connection Strings]
        public static SqlConnection MyAmdDBConnection = new SqlConnection(A2ZConfig.MyAmdDBConnectionString);

        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }

        public static void MyAmdOpenConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Closed)
            {
                MyAmdDBConnection.Open();
            }
        }
        public static void MyAmdCloseConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Open)
            {
                MyAmdDBConnection.Close();
            }
        }
        public static DataTable GetRecordFromTable(string query)
        {
            try
            {
                if (!string.IsNullOrEmpty(query))
                {
                    sqlCommand = new SqlCommand(query, MyAmdDBConnection);

                    sqlDataAdapter = new SqlDataAdapter();
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    dataSet = new DataSet();
                    sqlDataAdapter.Fill(dataSet, "T_Table");
                    dataTable = dataSet.Tables["T_Table"];
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static bool InsertUpdateDataBase(string query)
        {
            try
            {
                if (!string.IsNullOrEmpty(query))
                {
                    sqlCommand = new SqlCommand(query, MyAmdDBConnection);

                    MyAmdOpenConnection();
                    int isSuccess = sqlCommand.ExecuteNonQuery();
                    MyAmdCloseConnection();

                    if (isSuccess > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        #endregion
        public static DataTable GetAgencyDetailById(string agencyId)
        {
            try
            {
                sqlCommand = new SqlCommand("AgencyDetails", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", agencyId));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "agent_register");
                dataTable = dataSet.Tables["agent_register"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
        public static bool Information(string postUrl, string requestRemark, string requestJson, string responseJson, string actionType, string agentId, string clientRefId)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_Information_SMBPResponseLog", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@PostUrl", postUrl));
                sqlCommand.Parameters.Add(new SqlParameter("@RequestRemark", requestRemark));
                sqlCommand.Parameters.Add(new SqlParameter("@RequestJson", requestJson));
                sqlCommand.Parameters.Add(new SqlParameter("@ResponseJson", responseJson));
                sqlCommand.Parameters.Add(new SqlParameter("@ActionType", actionType));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", agentId));
                sqlCommand.Parameters.Add(new SqlParameter("@ClientRefId", clientRefId));

                MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool Error(string requestRemark, string postUrl, string requestJson, string responseRemark, string errorRemark, string actionType, string agentId, string clientRefId)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_Error_SMBPResponseLog", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@RequestRemark", requestRemark));
                sqlCommand.Parameters.Add(new SqlParameter("@PostUrl", postUrl));
                sqlCommand.Parameters.Add(new SqlParameter("@RequestJson", requestJson));
                sqlCommand.Parameters.Add(new SqlParameter("@ResponseRemark", responseRemark));
                sqlCommand.Parameters.Add(new SqlParameter("@ErrorRemark", errorRemark));
                sqlCommand.Parameters.Add(new SqlParameter("@ActionType", actionType));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", agentId));
                sqlCommand.Parameters.Add(new SqlParameter("@ClientRefId", clientRefId));

                MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static DataTable GetA2ZServiceCredential(string department, string service)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_GetA2ZServiceCredential", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Department", department));
                sqlCommand.Parameters.Add(new SqlParameter("@Service", service));

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataTable = new DataTable();
                sqlDataAdapter.Fill(dataTable);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }
    }
}
